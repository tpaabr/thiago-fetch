const $ = document
const url = "https://in-api-treinamento.herokuapp.com/posts"
const but = $.querySelector("#botao");
const loadBut = $.querySelector("#carregar")
const lista = $.querySelector("#lista");

//Botao para carregar
function activateFetch() {

    fetch(url).then((resp) => resp.json()).then((resp) => {
        let cor2 = 0
        for (let i = 0; i < resp.length; i++) {
            
            let currentObj = resp[i];

            let quadradoMaior = $.createElement("div");
            quadradoMaior.classList.add("quadradoMaior");

            let mostrarNome = $.createElement("p");
            let mostrarMensagem = $.createElement("p");
            let idEscondido = $.createElement("p");
            let remover = $.createElement("p")

            //Esconder ID
            idEscondido.style.display = "none"
            
    
            let currentName = currentObj.name
            let currentMessage = currentObj.message
            let currentId = currentObj.id
    
            mostrarNome.innerText = `Nome: ${currentName}`;
            mostrarMensagem.innerText = `Mensagem: ${currentMessage}`;
            idEscondido.innerText = `ID: ${currentId}`
            remover.innerText = "Remover"

            quadradoMaior.appendChild(mostrarNome)
            quadradoMaior.appendChild(mostrarMensagem)
            quadradoMaior.appendChild(idEscondido)
            quadradoMaior.appendChild(remover)
            

            lista.appendChild(quadradoMaior)
        }
    })
}


const myHeaders = {
    "content-Type":"application/json"
}

//Botao para enviar objeto
but.addEventListener("click", () => {
    let nome = $.querySelector("#nome").value;
    let mensagem = $.querySelector("#mensagem").value;

    let novoPost = {
        "post": {
            "name": nome,
            "message": mensagem
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(novoPost)

    }

    fetch(url, fetchConfig).then(console.log)

})

//Botao carregar
loadBut.addEventListener("click", () => activateFetch())

lista.addEventListener("click", (e) => {
    //Conseguir Novo ID para DELETE, pelo innerText da tag html
    let targetSquare = e.target.innerText
    let targetId = targetSquare[targetSquare.indexOf("ID") + 3];

    let newUrl = url + `/${targetId}`
    console.log(newUrl)

    let fetchConfig = {
        method: "DELETE",
        headers: myHeaders
    }

    fetch(newUrl, fetchConfig).then(console.log)
})






